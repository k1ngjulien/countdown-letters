package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// Uses a Trie to store all possible words
// https://en.wikipedia.org/wiki/Trie
// thanks to @jaffathecake on twitter for telling me about this
type node struct {
	children map[string]*node
	score    int
}

func newTrie() *node {
	return &node{
		make(map[string]*node),
		0,
	}
}

func insert(n *node, word string) {

	for _, char := range strings.Split(word, "") {

		if _, ok := n.children[char]; ok == false {
			n.children[char] = newTrie()
		}

		n = n.children[char]
	}

	n.score = len(word)
}

func find(n *node, key string) int {

	for _, char := range strings.Split(key, "") {

		if _, ok := n.children[char]; ok == true {
			n = n.children[char]
		} else {
			return 0
		}
	}

	return n.score
}

// thanks: https://yourbasic.org/golang/generate-permutation-slice-string/
// Perm calls f with each permutation of a.
func permFunc(a []rune, f func([]rune)) {
	perm(a, f, 0)
}

// Permute the values at index i to len(a)-1.
func perm(a []rune, f func([]rune), i int) {
	if i > len(a) {
		f(a)
		return
	}

	perm(a, f, i+1)
	for j := i + 1; j < len(a); j++ {
		a[i], a[j] = a[j], a[i]
		perm(a, f, i+1)
		a[i], a[j] = a[j], a[i]
	}
}

func main() {
	tree := newTrie()

	fmt.Println("Loading Wordlist...")
	file, err := os.Open("wordlist.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		text := scanner.Text()
		text = strings.Replace(text, "\n", "", -1)

		insert(tree, text)
	}

	highscore := 0
	minWordLenght := 3

	reader := bufio.NewReader(os.Stdin)

	// foundWords := map[int]map[string]struct{}{}
	found := []map[string]struct{}{}

	for i := 0; i < 9; i++ {
		found = append(found, map[string]struct{}{})
	}

	fmt.Println("Ready.")
	for {
		fmt.Print("-> ")
		input, _ := reader.ReadString('\n')
		input = strings.Replace(input, "\n", "", -1)

		// go over all permutations of the input
		permFunc([]rune(input), func(perm []rune) {

			// go over all possible words in this permutation
			for i := 0; i < len(perm)-minWordLenght; i++ {
				for j := len(perm); j >= i+minWordLenght; j-- {
					str := string(perm[i:j])
					score := find(tree, str)

					// store found words
					if score > 0 && score >= highscore {
						highscore = score

						if _, ok := found[score-1][str]; !ok {
							found[score-1][str] = struct{}{}
						}
					}
				}
			}
		})

		// print found words
		fmt.Println("Possible Words:")
		for score, words := range found {

			if len(words) > 0 {
				fmt.Printf("%v Letters: ", score+1)
				for w := range words {
					fmt.Printf("%v ", w)
				}
				fmt.Println()
			}
		}

		// reset state for the next input
		found := []map[string]struct{}{}

		for i := 0; i < 9; i++ {
			found = append(found, map[string]struct{}{})
		}

		highscore = 0
	}
}
